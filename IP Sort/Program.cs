﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace IP_Sort
{
    class Program
    {
        public class IP_Object
        {
            public string original_ip;
            public double parsed_value;
        }
        public class File_Manager
        {
            public string GetSaveLocation()
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Title = "Choose File Name & Location";
                save.Filter = "Text Files (.txt)|*.txt";
                save.DefaultExt = "txt";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    return save.FileName.ToString();
                }
                else
                    return "";

            }
            public List<string> GetSelectedTextFiles()
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Open Text Files";
                dialog.Filter = "Text Files (.txt)|*.txt";
                dialog.Multiselect = true;
                dialog.RestoreDirectory = true;
                List<string> returnFiles = new List<string>();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    foreach (String file in dialog.FileNames)
                    {
                        returnFiles.Add(file);
                    }
                }
                return returnFiles;
            }
        }
        public class Sort: File_Manager
        {
            public void start()
            {
                Console.WriteLine("Start");
                List<string> getFiles = GetSelectedTextFiles();
                List<IP_Object> ipList = new List<IP_Object>();
                foreach(string file in getFiles)
                {
                    List<IP_Object> newIPs = parseFile(file);
                    ipList.AddRange(newIPs);
                }
                var numeric = ipList.OrderBy(a => a.parsed_value);
                foreach(IP_Object output in numeric)
                {
                    Console.WriteLine(output.original_ip);
                }
                writeOutput(numeric);
                Console.WriteLine("COMPLETE!");
                Console.ReadLine();
            }
           
            private List<IP_Object> parseFile(string fileName)
            {
                List<IP_Object> newList = new List<IP_Object>();
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                String line;
                while ((line = file.ReadLine()) != null)
                {
                    IP_Object newObject = new IP_Object();
                    newObject.original_ip = line;
                    //Parse IP Address
                    newObject.parsed_value = parseIp(line);
                    newList.Add(newObject);                                                  
                }

                file.Close();
                return newList;
            }
            //Parse the ip address into an integer value. 
            private double parseIp(string line)
            {
                string[] split = line.Split('.');
                int part = 3;
                double sum = 0;
                foreach(string octet in split)
                {
                    int numeric;
                    if(Int32.TryParse(octet, out numeric))
                    {
                        if (part != 0)
                            sum += numeric * (Math.Pow(255, part--)); //Max value 255
                        else
                            sum += numeric;
                    }
                }
               
                return sum;
            }

            private void writeOutput(IEnumerable<IP_Object> output)
            {
                string fileName = GetSaveLocation();
                if (fileName != "")
                {
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fileName))
                    {
                        foreach (IP_Object newLine in output)
                        {
                            writer.WriteLine(newLine.original_ip);
                        }
                    }
                    System.Diagnostics.Process.Start(fileName);
                }
                else
                {
                    Console.WriteLine("No save file selected, file not saved.");
                }
                
            }

        }

        [STAThread]
        static void Main(string[] args)
        {
    
            Sort newSort = new Sort();
            newSort.start();
          
        }
    }
}
